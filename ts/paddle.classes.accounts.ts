import * as plugins from './paddle.plugins';

export  class PaddleAccount {
  private apiKey: string

  constructor(apiKeyArg: string) {
    this.apiKey = apiKeyArg;
  }
}